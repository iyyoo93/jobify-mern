import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.main`
  div {
    background: red;
  }
  h1 {
    color: green;
  }
`;

const Landing = () => {
  return (
    <Wrapper>
      <h1>Landing</h1>
      <div>Check</div>
    </Wrapper>
  );
};

export default Landing;
